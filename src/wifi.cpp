#include <Arduino.h>
#include <MQTT.h>
#include <WiFi.h>
#include "global.hpp"
#include "wifi.hpp"

long getStrength (int points) {
    long rssi = 0;
    long averageRSSI = 0;
    
    for (int i = 0; i < points; i++) {
        rssi += WiFi.RSSI();
        delay(20);
    }

   averageRSSI = rssi / points;
   
   return averageRSSI;
}


uint8_t rssi2percentage (int rssi) {
  // RSSI is not linear, then:

  uint8_t perc = 0;
  
  if (rssi >= -50) {
    perc = 100;
  } else if ((rssi < -50) && (rssi >= -55)) {
    perc = 90;
  } else if ((rssi < -55) && (rssi >= -62)) {
    perc = 80;
  } else if ((rssi < -62) && (rssi >= -63)) {
    perc = 75;
  } else if ((rssi < -63) && (rssi >= -65)) {
    perc = 70;
  } else if ((rssi < -65) && (rssi >= -70)) {
    perc = 60;
  } else if ((rssi < -70) && (rssi >= -79)) {
    perc = 50;
  } else if ((rssi < -79) && (rssi >= -82)) {
    perc = 30;
  } else if ((rssi < -82) && (rssi >= -87)) {
    perc = 20;
  } else if (rssi < -87) {
    perc = 10;
  }

  return perc;
}

void check_WiFi_quality (MQTTClient &client) {
  
  // Get the RSSI (dbm):
  int rssi = getStrength(RSSI_POINTS);
  
  // Convert dbm value to percentage:
  int wifi_quality = rssi2percentage(rssi);

  // Publish MQTT msg: 
  client.publish(String(MQTT_MACHINE_WIFI_PATH), String(wifi_quality));
}



