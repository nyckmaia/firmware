#include <Arduino.h>
#include <MQTT.h>

#define MQTT_MSG_BUF_SIZE 4096 // Default: 4096 bytes
#define PORT 1883

void connect();
void messageReceived(String &topic, String &payload);