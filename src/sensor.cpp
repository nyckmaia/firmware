#include <Arduino.h>
#include <MQTT.h>
#include "global.hpp"
#include "sensor.hpp"

void dump_sensor_info(Sensor *sensor) {
  for (int i = 0; i < num_sensors; i++) {
        String sensor_info = "sensor_id: ";
        sensor_info += String(sensor[i].id);
        sensor_info += " | ";
        sensor_info += sensor[i].name;
        sensor_info += " => connected_gpio: ";
        sensor_info += String(sensor[i].gpio);
        Serial.println(sensor_info);
      }
}


void get_init_sensor_values(MQTTClient &client, Sensor *sensor) {

  // Read all sensors:
  for (int i = 0; i < num_sensors; i++) {
    // Read GPIO pin:
    sensor[i].value = digitalRead(sensor[i].gpio);

    // Check if the initial sensor state is actived:
    if(sensor[i].value == true) {
      // Publish MQTT msg: Machine turned on with sensor errors:
      client.publish(String(MQTT_SENSOR_PATH + String(sensor[i].id)), String(sensor[i].value));
    }  
  }  
}


void check_sensors_change(MQTTClient &client, Sensor *sensor) {
  bool temp_value;
  
  // Read all sensors:
  for (int i = 0; i < num_sensors; i++) {
    // Read GPIO pin:
    temp_value = digitalRead(sensor[i].gpio);

    // Check if the sensor has changed his value:
    if (temp_value != sensor[i].value) {
      // Store the new sensor value:
      sensor[i].value = temp_value;

      // Publish MQTT msg: Sensor value
      client.publish(String(MQTT_SENSOR_PATH + String(sensor[i].id)), String(sensor[i].value));
    }
  }

  // Get START, STOP and TIP buttons status:
  for (int i = 0; i < num_sensors; i++) {
    if(strcmp(sensor[i].name, "Botao START") == 0) {
      btn_start_status = sensor[i].value;
    } else if (strcmp(sensor[i].name, "Botao TIP") == 0) {
      btn_tip_status = sensor[i].value;
    } else if (strcmp(sensor[i].name, "Botao STOP") == 0) {
      btn_stop_status = sensor[i].value;
    }
  }

  // CASE 1: Check if the operador pressed the STOP button:
  if((btn_start_status == false) && (btn_stop_status == true) && (btn_tip_status == false) && (machine_state == Machine_State::IN_PRODUCTION)) {
    // Operador has stoped this machine!
    Serial.println("MACHINE HAS STOPED!");

    machine_state = Machine_State::STOPPED_BY_USER;

    // Publish MQTT msg: Machine State
    client.publish(String(MQTT_MACHINE_STATE_PATH), String(machine_state));
  }

  // CASE 2: Check if START and STIP buttons are both actived and if the machine is stoped:
  if ((btn_start_status == true) && (btn_stop_status == false) && (btn_tip_status == true) && (machine_state == Machine_State::STAND_BY)) {
    // Operador has started this machine!
    Serial.println("MACHINE HAS STARTED!");

    machine_state = Machine_State::IN_PRODUCTION;

    // Publish MQTT msg: Machine State
    client.publish(String(MQTT_MACHINE_STATE_PATH), String(machine_state));
  }
}