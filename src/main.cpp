#define ARDUINOJSON_ENABLE_ARDUINO_STRING 1

#include <Arduino.h>
#include <WiFi.h>
#include <MQTT.h>

#include "global.hpp"
#include "wifi.hpp"
#include "mqtt.hpp"
#include "sensor.hpp"
#include "hall_effect.hpp"
#include "main.hpp"

// Create a sensor object array:
Sensor sensor[NUM_MAX_SENSORS];

bool is_received_config = false;
bool is_gpio_set = false;

int num_sensors = 0;

bool btn_start_status = false; 
bool btn_tip_status = false;
bool btn_stop_status = false;


WiFiClient net;
MQTTClient client (MQTT_MSG_BUF_SIZE);

void setup() {

  // Init Serial:
  Serial.begin(115200);

  // WiFi config:
  WiFi.begin(ssid, pass);
  client.begin(ip, net);
  client.onMessage(messageReceived);
  connect();

  // Request the server configs: 
  client.publish(String(MQTT_CONFIG_REQUEST_PATH), String(MY_ID));
}

void loop() {
  client.loop();

  if (!client.connected()) {
    connect();
  }

  // ==== Init configs =====
   
  // Received MQTT config:
  if(is_received_config) {
    // Check pinMode configs:
    if(!is_gpio_set) {
      // Configure GPIO:
      for(int i = 0; i < num_sensors; i++) {
        // Set pin as INPUT:
        pinMode(sensor[i].gpio, INPUT);
      }
      
      // Set the gpio config flag to true:
      is_gpio_set = true;

      Serial.print("==== Number of actived sensors: ");
      Serial.println(num_sensors);

      // Print sensors info:
      dump_sensor_info(sensor);
      
      // Get all initial sensors values:
      get_init_sensor_values(client, sensor);

      // Publish MQTT Msg: This machine was turned on, but wait instructions:
      machine_state = Machine_State::STAND_BY;
      client.publish(String(MQTT_MACHINE_STATE_PATH), String(machine_state));
      Serial.println("==== Waiting for instructions! ====");
    }

    // ====== All configs Ok! ====== 

    
    // Read Wifi signal:
    check_WiFi_quality(client);
    
    // Read all sensors:
    check_sensors_change(client, sensor);
    
    // Read internal Hall Sensor:
    check_hall_effect(client);
  }
  
}