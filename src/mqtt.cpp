#include <Arduino.h>
#include <WiFi.h>
#include <MQTT.h>
#include <ArduinoJson.h>
#include "global.hpp"


void connect() {
  Serial.print("Checking WiFi: ");
  Serial.print(ssid);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }

  Serial.print("\n");

  String str = String(String ("Trying to connect: " + String(ip)) + String(":" + String(port)));
  Serial.print(str);
  while (!client.connect(ip, port)) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nConnected!");

  client.subscribe(String(MQTT_CONFIG_RESPONSE_PATH));
}

void messageReceived(String &topic, String &payload) {

  if(topic == String(MQTT_CONFIG_RESPONSE_PATH)) {
  //if(strcmp(topic, MQTT_CONFIG_RESPONSE_PATH) == 0) {
    Serial.print("Setting GPIO's...");

    StaticJsonBuffer<4096> jsonBuffer;

    JsonArray& root = jsonBuffer.parseArray(payload);

    // Test if parsing succeeds.
    if (!root.success()) {
      Serial.println("parseArray() failed");
      return;
    } else {
      // Json Parse: SUCCESS:

      // Get the number of sensors from the received json:
      num_sensors = root.size();
    
      // Get data for each sensor:
      for (int i = 0; i < num_sensors; i++) {
        // === Put MQTT msg into Sensor Object array ===
        // Set the sensor name:
        memset(sensor[i].name, 0, sizeof(sensor[i].name));
        strcpy(sensor[i].name, root[i]["name"]);
        // Set the sensor id:
        sensor[i].id = root[i]["sensor_id"];
        // Set the sensor connected GPIO:
        sensor[i].gpio = root[i]["connected_gpio"];
        // Set the sensor value to false:
        sensor[i].value = false;
      }
    
      // Set flag to true: 
      is_received_config = true;
      Serial.println("Ok!");
    }
  } else {
    // Unknown topic:
    Serial.println("incoming: " + topic + " - " + payload);
  }
}
