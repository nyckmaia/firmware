#include <Arduino.h>

#define WIFI_QUALITY_DELTA 10
#define WIFI_QUALITY_STABLE_LEVEL 50
#define RSSI_POINTS 20

void check_WiFi_quality (MQTTClient &client);
