#include <Arduino.h>
#include "global.hpp"
#include "hall_effect.hpp"

void check_hall_effect(MQTTClient &client) {

  // Init variables:
  int hall_value, avg_value;
  hall_value = avg_value = 0;
  
  int num_meassurements = 5;
  
  // Read internal ESP32  Hall Sensor:
  for (int i = 0; i < num_meassurements; i++) {
    hall_value += hallRead();
    delay(20);
  }

  // Compute the average of meassurements:
  avg_value = hall_value / num_meassurements;

  // Publish MQTT msg: 
  client.publish(String(MQTT_BASE_PATH + String("/hall")), String(avg_value));
}

