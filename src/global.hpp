#ifndef __global_h__
#define __global_h__

#include <Arduino.h>
#include <stdint.h>
#include <string.h>
#include <MQTT.h>

#define XSTR(x) #x
#define STR(x) XSTR(x)

// Machine ID:
#define MY_ID 1

// MQTT sub string:
#define MQTT_BASE gitex/tear/
#define MQTT_CONFIG_REQUEST /config_request
#define MQTT_CONFIG_RESPONSE /config_response
#define MQTT_MACHINE_STATE /state
#define MQTT_MACHINE_WIFI /wifi
#define MQTT_SENSOR /sensor/

// MQTT FULL PATH:
#define MQTT_BASE_PATH STR(MQTT_BASE) STR(MY_ID)
#define MQTT_CONFIG_REQUEST_PATH MQTT_BASE_PATH STR(MQTT_CONFIG_REQUEST)
#define MQTT_CONFIG_RESPONSE_PATH MQTT_BASE_PATH STR(MQTT_CONFIG_RESPONSE)
#define MQTT_MACHINE_STATE_PATH MQTT_BASE_PATH STR(MQTT_MACHINE_STATE)
#define MQTT_MACHINE_WIFI_PATH MQTT_BASE_PATH STR(MQTT_MACHINE_WIFI)
#define MQTT_SENSOR_PATH MQTT_BASE_PATH STR(MQTT_SENSOR)

// Max number of sensors:
#define NUM_MAX_SENSORS 16

// Create the data type Sensor:
typedef struct sensor_st {
    uint8_t id;
    uint8_t gpio;
    bool value;
    char name[30];
} Sensor;

// Machine state (from db)
enum Machine_State {
    STAND_BY = 1,
    IN_PRODUCTION,
    STOPPED_BY_USER,
    WAITING_MAINTENANCE,
    UNDER_MAINTENANCE
};

extern Machine_State machine_state;

extern MQTTClient client;

extern Sensor sensor[NUM_MAX_SENSORS];
extern int num_sensors;

// MQTT:
const char ssid[] = "Nyck Rede (2.4GHz)";
const char pass[] = "bach12345";
const char ip[] = "192.168.1.102";
const char port[] = "1883";

// Configs:
extern bool is_received_config;
extern bool is_gpio_set;

// Status:
extern bool btn_start_status, btn_tip_status, btn_stop_status;


#endif