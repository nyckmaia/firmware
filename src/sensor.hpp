#include <MQTT.h>
#include "global.hpp"

void dump_sensor_info(Sensor *sensor);
void get_init_sensor_values(MQTTClient &client, Sensor *sensor);
void check_sensors_change(MQTTClient &client, Sensor *sensor);